﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

	public float charSpeed;
	public float jumpHeight;
	public AudioClip jumpSound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey ("d")){
			this.rigidbody2D.velocity = new Vector2(charSpeed,this.rigidbody2D.velocity.y);
		}
		if(Input.GetKey ("a")){
			this.rigidbody2D.velocity = new Vector2(-charSpeed,this.rigidbody2D.velocity.y);
		}
		if(this.rigidbody2D.velocity.y == 0f){
			if(Input.GetKey ("space") || Input.GetKey("w")){
				this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x,jumpHeight);
				audio.PlayOneShot(jumpSound);
			}
		}
	}
}
